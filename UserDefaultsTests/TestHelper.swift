//
//  TestHelper.swift
//  UserDefaultsTests
//
//  Created by Sergio Andres Rodriguez Castillo on 15/12/23.
//

import Foundation
import UIKit

func tap(_ button: UIButton) {
    button.sendActions(for: .touchUpInside)
}
