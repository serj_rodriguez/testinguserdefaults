//
//  FakeUserDefaults.swift
//  UserDefaultsTests
//
//  Created by Sergio Andres Rodriguez Castillo on 15/12/23.
//

import Foundation
@testable import UserDefaults

final class FakeUserDefaults: UserDefaultsProtocol {
    var integers: [String: Int] = [:]
    
    func set(_ value: Int, forKey defaultName: String) {
        integers[defaultName] = value
    }
    
    func integer(forKey defaultName: String) -> Int {
        integers[defaultName] ?? 0
    }    
}
